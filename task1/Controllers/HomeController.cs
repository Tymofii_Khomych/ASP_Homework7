﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using task1.Models;

namespace task1.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index(RegistrationBindingModel model)
        {
            return View(model);
        }
    }
}