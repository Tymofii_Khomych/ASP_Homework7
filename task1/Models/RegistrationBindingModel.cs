﻿namespace task1.Models
{
    public class RegistrationBindingModel
    {
        public string First { get; set; }
        public string Second { get; set; }
        public int Count { get; set; }
    }
}
